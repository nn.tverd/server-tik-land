const mongoose = require("mongoose");
// const shortId = require("shortid");

const userSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
            max: 32,
        },
        email: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            lowercase: true,
        },
        password: {
            type: String,
            required: true,
        },
        resetLink: {
            type: String,
            default: "",
        },
        accountSum: {
            type: Number,
            default: 0,
        },
        
    },
    { timestamps: true }
);

module.exports = mongoose.model("user", userSchema);
