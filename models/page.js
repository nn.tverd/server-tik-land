const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// const shortId = require("shortid");

const pageSchema = Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            required: true,
        },
        landing: {
            type: Schema.Types.ObjectId,
            required: true,
        },
        isActive: { type: Boolean, default: true },
        pageName: { type: String, max: 24, default: "" },
        pageDescription: { type: String, max: 1024, default: "" },
        accname: { type: String, max: 24, default: "" },
        description: { type: String, max: 256, default: "" },
        songIcon: { type: String, max: 24, default: "standart" },
        songTitle: { type: String, max: 24, default: "" },
        topAlert: { type: String, max: 24, default: "" },
        showSliderHint: { type: Boolean, default: false }, // true false
        type: { type: String, max: 10, default: "video" }, // video, image and/or h1&h2, custom html,
        videoUrl: { type: String, max: 512, default: "" },
        // videoUrl: "bullets2/bulet4 ошибка 7.mp4", // if type=video
        useYoutubeCover: { type: Boolean, default: false },
        coverUrl: { type: String, max: 512, default: "" },
        // coverUrl: "",
        imgUrl: { type: String, max: 512, default: "" }, // if type=img
        bgColor: { type: String, max: 24, default: "" }, // if type=img
        showTextSections: { type: Boolean, default: false },
        text1Height: { type: Number, default: 3 },
        text2Height: { type: Number, default: 2 },
        text3Height: { type: Number, default: 1 },
        header1: { type: String, max: 256, default: "" }, // if type=img
        header2: { type: String, max: 256, default: "" }, // if type=img
        header3: { type: String, max: 256, default: "" }, // if type=img
        text1FontSize:{ type: Number, default: 1 },
        text2FontSize:{ type: Number, default: 1 },
        text3FontSize:{ type: Number, default: 1 },
        text1FontColor: { type: String, max: 256, default: "black" },
        text2FontColor: { type: String, max: 256, default: "black" },
        text3FontColor: { type: String, max: 256, default: "black" },
        text1UseGlitch: { type: Boolean, default: true },
        text2UseGlitch: { type: Boolean, default: false },
        text3UseGlitch: { type: Boolean, default: false },
        
        
        html: {
            type: String,
            default: "<style></style><html></html><script></script>",
        }, // if type=html
        css: { type: String, default: "" }, // if type=html
        iconForSidebar: { type: String, max: 24, default: "" },
        showMainActionSideButtons: { type: Boolean, default: false },
        
        isLeftButtonVisible: { type: Boolean, default: true },
        isRightButtonVisible: { type: Boolean, default: true },
        isCentralButtonVisible: { type: Boolean, default: true },
        
        leftButtonIcon: { type: String, max: 24, default: "" },
        rightButtonIcon: { type: String, max: 24, default: "" },
        centralButtonIcon: { type: String, max: 24, default: "" },
        //
        lawData: { type: String, max: 1024, default: "" }, // inn ogrn contacty
        leftButtonText: { type: String, max: 1024, default: "" }, // inn ogrn contacty
        //
        rightButtonText: { type: String, max: 1024, default: "" },
        //
        centralButtonText: { type: String, max: 24, default: "Go to shop" },
        buttonText: { type: String, max: 24, default: "Go to shop" },
        //
        rightButtonLink: { type: String, max: 512, default: "" },
        linkFromSideBar: { type: String, max: 512, default: "" },
        //
        centralButtonLink: { type: String, max: 512, default: "" },
        buttonUrl: { type: String, max: 512, default: "" },
        //
        centralButtonType: { type: String, max: 24, default: "light" }, // arrow, light, solid
        buttonType: { type: Number, default: -1 }, // 0 hidden / 1 outline white / 2 red filled
        
        //
        avatarUrl: { type: String, max: 512, default: "" },
        likes: { type: Number, default: 0 },
        shares: { type: Number, default: 0 },
        showLikes: { type: Boolean, default: true },
        showShares: { type: Boolean, default: true },
        
        
        showSidebarButton: { type: Boolean, default: false },
        sideBarIcon: { type: String, max: 24, default: "standart" },
        sideBarMessage: { type: String, max: 256, default: "" },
        sideBarBlick: { type: Boolean, default: false },
        sideBarUrl: { type: String, max: 512, default: "" },
        sideBarTitle: { type: String, max: 6, default: "" },
    },
    { timestamps: true }
);

module.exports = mongoose.model("page", pageSchema);
