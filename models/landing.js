const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// const shortId = require("shortid");

const landingSchema = mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            max: 128,
        },
        user: {
            type: Schema.Types.ObjectId,
        },
        description: {
            type: String,
            trim: true,
            default: "",
        },
        status: {
            type: String, // "ACTIVE" - active, PAUSED - paused, DELETED - deleted
            default: "PAUSED",
        },
        pagesCount: {
            type: Number,
            default: 0,
        },
        pages: [
            {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: "page",
                },
                order: Number,
            },
        ],
        isPromoted: {
            type: Boolean,
            default: false,
        },
        avatarUrl:{
            type: String,
            default: ""
        },
        totalCost: {
            type: Number,
            default: 0,
        },
        lastDateOfPay: {
            type: Date,
            default: Date.UTC(1970, 1, 1, 0, 0, 0, 0),
        },
        lastAmountOfPay: {
            type: Number,
            default: 0,
        },
        notEnoughMoney:{
            type: String,
            default: ""
        },
        shareTitle:{
            type: String,
            default: ""
        },
        shareQuota:{
            type: String,
            default: ""
        },
        shareHashtags:{
            type: String,
            default: ""
        },
        shareUrl:{
            type: String,
            default: ""
        },
    },
    { timestamps: true }
);

module.exports = mongoose.model("landing", landingSchema);
