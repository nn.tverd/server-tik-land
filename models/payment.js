const mongoose = require("mongoose");
// const shortId = require("shortid");

const paymentSchema = mongoose.Schema(
    {
        userId: {
            type: String,
        },
        amount: {
            type: Number,
        },
        currency: {
            type: String,
        },
        status: {
            type: String,
        },
        payload: {
            type: Object,
        },
    },
    { timestamps: true }
);

module.exports = mongoose.model("payment", paymentSchema);
