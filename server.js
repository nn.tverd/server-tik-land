const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv").config();
require("./db/connectDb.js");

// const ShortUrl = require("./models/shortUrl.js");

// nikolay
// 7N94jcJ4Aajhcs5

const app = express();

app.use(cors());

// app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: "50mb", extended: true }));

const userRouter = require("./routers/userRouter.js");
app.use("/user", userRouter);

const landingRouter = require("./routers/landingRouter.js");
app.use("/landing", landingRouter);

const pageRouter = require("./routers/pageRouter.js");
app.use("/page", pageRouter);

const paymentRouter = require("./routers/paymentRouter.js");
app.use("/payment", paymentRouter);

const port = process.env.PORT || 3080;

app.listen(port, () => console.log("server is running on " + port));
