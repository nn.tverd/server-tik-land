const jwt = require("jsonwebtoken");


const verifyJWT = (req, res, next)=>{
    const token = req.headers['x-access-token'];

    if(!token){
        return res.status(400).json({error: "You have to be authorized!"})
    }

    jwt.verify(token, process.env.JWT_SIGNIN_TOKEN, (err, decoded)=>{
        if(err){
            return res.json({error: "Request failed! Either token wrong or it has expired!"})
        }
        req.userId = decoded._id;
        next();
    })
}

module.exports = verifyJWT;