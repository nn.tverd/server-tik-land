// const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const shortId = require("shortid");
const verifyJWT = require("./verifyJwt.js");
const fetch = require("node-fetch");
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const Landing = require("../models/landing.js");
const User = require("../models/user.js");
const Page = require("../models/page.js");
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// **********************************************************************************************
// 196001869120-r8at099bqvb727793pe27erplth0as6o.apps.googleusercontent.com

// **********************************************************************************************
router.get("/get/:landingId", async (req, res) => {
    try {
        const { landingId } = req.params;
        const landing = await Landing.findOne({ _id: landingId });
        console.log(landing);
        if (_.isEmpty(landing))
            return res
                .status(404)
                .json({ error: "We cannot find landing with this id" });
        console.log(`if (landing.status != "ACTIVE")`, landing.status);
        if (landing.status !== "ACTIVE")
            return res
                .status(404)
                .json({ error: "A landing owner has switched it off" });
        const pages = await Page.find({ landing: landingId, isActive: true });
        if (!pages)
            return res.status(404).json({
                error:
                    "A landing owner has not added any active page to this landing.",
            });
        return res.status(200).json({
            message: "Data loaded successfully.",
            payload: { landing: landing, pages: pages },
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ error: "Error loading landing" });
    }
});

router.post("/createLanding", verifyJWT, async (req, res) => {
    const { name, description, user } = req.body;

    const newLanding = new Landing({
        name: name,
        description: description,
        user: user,
    });
    try {
        const result = await newLanding.save();
        console.log(result);
        return res.status(200).json({ message: "Landing was added" });
    } catch (error) {
        console.log(error);
        return res
            .status(400)
            .json({ error: "Error during creating new Landing" });
    }
});

router.post("/getLandings", verifyJWT, async (req, res) => {
    const { userId } = req.body;
    console.log("/getLandings", userId);
    try {
        const landings = await Landing.find({ user: userId });
        console.log("/getLandings", "landings length", landings.length);
        return res
            .status(200)
            .json({ message: "List of landing was got", payload: landings });
    } catch (error) {
        return res
            .status(400)
            .json({ error: "Error during getting list of landings" });
    }
});

router.post("/getOneLanding", verifyJWT, async (req, res) => {
    const { userId, landingId } = req.body;
    console.log("/getOneLanding", userId, landingId);
    try {
        const landing = await Landing.findOne({ user: userId, _id: landingId });
        console.log("/getOneLanding", "landing length", landing);
        return res
            .status(200)
            .json({ message: "We got landing", payload: landing });
    } catch (error) {
        return res.status(400).json({ error: "Error during getting landing" });
    }
});

router.post("/saveOneLanding", verifyJWT, async (req, res) => {
    const { userId, landingId, payload } = req.body;
    console.log("/saveOneLanding", userId, landingId);
    try {
        let landing = await Landing.findOne({ user: userId, _id: landingId });
        if (!landing) {
            return res
                .status(400)
                .json({ error: "Error during saving landing" });
        }
        console.log("/saveOneLanding", "landing length 1", landing);
        landing = _.extend(landing, payload);
        await landing.save();
        console.log("/saveOneLanding", "landing length 2", landing);
        return res
            .status(200)
            .json({ message: "We got landing", payload: landing });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ error: "Error during saving landing" });
    }
});

router.post("/checkLink", verifyJWT, async (req, res) => {
    const { landingId, link } = req.body;
    console.log("/checkLink", landingId);
    let includesTwitterOrFacebook = false;
    // if (link.includes("twitter")) {
    //     includesTwitterOrFacebook = true;
    // }
    if (link.includes("facebook") /*|| link.includes("twitter")*/) {
        includesTwitterOrFacebook = true;
    }
    if (!includesTwitterOrFacebook) {
        return res.status(401).json({
            error:
                "This link probably not from Facebook or Twitter. Please contact me via Telegram to verify it",
            // "This link probably not from Facebook or Twetter. Please contact me via Telegram to verify it",
        });
    }
    try {
        const _post = await fetch(
            "https://tik-land-link-checker.herokuapp.com/linkcheck",
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    linkToPost: link,
                }),
            }
        );
        const ____post = await _post.json();
        if (!____post.html) {
            throw "html does not received";
        }
        const post = ____post.html;
        console.log("post", post, landingId);
        let message = "Post was found. ";
        if (!post.includes(landingId)) {
            return res.status(401).json({
                error: message + "But id was not found on the page",
            });
        }
        message += "Id was found. ";

        if (!post.includes("f")) {
            // if (!post.includes("netlify")) {
            return res.status(401).json({
                error: message + "But link was not found on the page",
            });
        }
        message += "Link on a service was found. ";
        if (!post.includes("#tiktok")) {
            return res.status(401).json({
                error: message + "Hashtag was not found",
            });
        }
        message += "Hashtag was found. ";
        if (!post.toLowerCase().includes("candy")) {
            return res.status(401).json({
                error:
                    message + "Name of the service was not found in the post",
            });
        }
        message += "Name of service was found. Check is completed. ";

        let landing = await Landing.findOne({ _id: landingId });
        if (!landing) {
            return res.status(400).json({
                error: message + " Error findingLanding saving landing",
            });
        }
        console.log("/checkLink", "landing length 1", landing);
        landing = _.extend(landing, { isPromoted: true });
        await landing.save();
        console.log("/checkLink", "landing length 2", landing);
        return res
            .status(200)
            .json({ message: message + " We got promotions" });
    } catch (error) {
        console.log(error);
        return res
            .status(400)
            .json({ error: "Error during checking post on social media" });
    }
});

router.get("/checkPayments", async (req, res) => {
    console.log("checkPayments");
    try {
        const landings = await Landing.find({ status: "ACTIVE" });
        for (let l in landings) {
            const landing = landings[l];
            const { lastAmountOfPay, lastDateOfPay, totalCost } = landing;
            if (!totalCost) continue;
            const lDay = new Date(lastDateOfPay);
            const now = new Date();
            const DAY_IN_MSEC = 86400000;
            const msec = now - lDay;
            console.log(msec);
            if (msec > DAY_IN_MSEC) {
                //charge
                // set new date
                const userId = landing.user;
                const user = await User.findOne({ _id: userId });
                if (user.accountSum - totalCost < 0) {
                    //block
                    landing.status = "PAUSED";
                    console.log("notEnoughMoney");
                    landing.notEnoughMoney =
                        "There is no enough money on your account. Please make payment, then relaunch landing page.";
                    await landing.save();
                    continue;
                } else {
                    user.accountSum -= totalCost;
                    landing.notEnoughMoney = "";
                    landing.lastDateOfPay = now;
                    landing.lastAmountOfPay = totalCost;

                    await landing.save();
                    await user.save();
                    continue;
                }
            } else {
                if (totalCost > lastAmountOfPay) {
                    // charge extra
                    // leave the same date
                    const userId = landing.user;
                    const user = await User.findOne({ _id: userId });
                    if (user.accountSum - totalCost + lastAmountOfPay < 0) {
                        //block
                        landing.status = "PAUSED";
                        console.log("notEnoughMoney");
                        landing.notEnoughMoney =
                            "There is no enough money on your account. Please make payment, then relaunch landing page.";
                        await landing.save();
                        continue;
                    } else {
                        user.accountSum -= totalCost + lastAmountOfPay;
                        landing.notEnoughMoney = "";
                        // landing.lastDateOfPay = now;
                        landing.lastAmountOfPay = totalCost;

                        await landing.save();
                        await user.save();
                        continue;
                    }
                }
            }
        }
        return res.status(200).json({ message: "payment checkout went ok!" });
    } catch (error) {
        console.log(error);
        return res
            .status(500)
            .json({ error: "internal error payment checking", payload: error });
    }
});

router.post("/checkPayment", async (req, res) => {
    console.log("\n\n\n\n\n\ncheckPayment post");
    const { landingId } = req.body;
    console.log("checkPayment post", landingId);
    try {
        const landing = await Landing.findOne({ _id: landingId });
        console.log("checkPayment post", landingId);
        const { lastAmountOfPay, lastDateOfPay, totalCost } = landing;

        if (!totalCost)
            return res.status(200).json({
                message: "payment checkout went ok!",
                payload: landing,
            });
        const lDay = new Date(lastDateOfPay);
        const now = new Date();
        const DAY_IN_MSEC = 86400000;
        const msec = now - lDay;
        console.log(msec);
        if (msec > DAY_IN_MSEC) {
            //charge
            // set new date
            const userId = landing.user;
            const user = await User.findOne({ _id: userId });
            if (user.accountSum - totalCost < 0) {
                //block
                landing.status = "PAUSED";
                console.log("notEnoughMoney");
                landing.notEnoughMoney =
                    "There is no enough money on your account. Please make payment, then relaunch landing page.";
                await landing.save();
                return res.status(200).json({
                    message: "payment checkout went ok!",
                    payload: landing,
                });
            } else {
                user.accountSum -= totalCost;
                landing.notEnoughMoney = "";
                landing.lastDateOfPay = now;
                landing.lastAmountOfPay = totalCost;

                await landing.save();
                await user.save();
                return res.status(200).json({
                    message: "payment checkout went ok!",
                    payload: landing,
                });
            }
        } else {
            if (totalCost > lastAmountOfPay) {
                // charge extra
                // leave the same date
                const userId = landing.user;
                const user = await User.findOne({ _id: userId });
                if (user.accountSum - totalCost - lastAmountOfPay < 0) {
                    //block
                    landing.status = "PAUSED";
                    console.log("notEnoughMoney");
                    landing.notEnoughMoney =
                        "There is no enough money on your account. Please make payment, then relaunch landing page.";
                    await landing.save();
                    return res.status(200).json({
                        message: "payment checkout went ok!",
                        payload: landing,
                    });
                } else {
                    console.log(
                        "user.accountSum -= totalCost + lastAmountOfPay; 2",
                        user.accountSum,
                        totalCost + lastAmountOfPay
                    );
                    user.accountSum -= totalCost - lastAmountOfPay;
                    console.log(
                        "user.accountSum -= totalCost + lastAmountOfPay; 2",
                        user.accountSum,
                        totalCost + lastAmountOfPay
                    );
                    landing.notEnoughMoney = "";
                    // landing.lastDateOfPay = now;
                    landing.lastAmountOfPay = totalCost;

                    await landing.save();
                    await user.save();
                    return res.status(200).json({
                        message: "payment checkout went ok!",
                        payload: landing,
                    });
                }
            }
        }
        return res
            .status(200)
            .json({ message: "payment checkout went ok!", payload: landing });
    } catch (error) {
        console.log(error);
        return res
            .status(500)
            .json({ error: "internal error payment checking", payload: error });
    }
});

router.post("/getPage", async (req, res) => {
    // router.post("/getPage", async (req, res) => {
    try {
        const { landingId } = req.body;
        let { offset, limit } = req.body;
        if (!offset) offset = 0;
        if (!limit) limit = 3;
        const landing = await Landing.findOne({ _id: landingId });
        // console.log("/getOneLanding", "landing length", landing);
        if (!landing) {
            return res.status(404).json({
                error: "Landing page with this id is not found",
                payload: error,
            });
        }
        if (landing.status !== "ACTIVE")
            return res
                .status(404)
                .json({ error: "A landing owner has switched it off" });
        const { pages } = landing;
        const sortedPages = _.orderBy(pages, ["order"], ["asc"]);
        // console.log(sortedPages, pages);
        const pagesIdsArray = sortedPages.map((page) => page._id);
        const promotedPages = await Page.find({ _id: { $in: pagesIdsArray } });
        // console.log(promotedPages);
        for (let i in sortedPages) {
            const lookingId = sortedPages[i]._id;
            const pp = _.find(promotedPages, ["_id", lookingId]);
            sortedPages[i] = _.extend(sortedPages[i], pp);
        }
        const activePages = _.filter(sortedPages, ["isActive", true]);
        // if (offset + limit >= activePages.length) {
        //     limit = activePages.length - 1 - offset;
        //     if (limit < 0) limit = 0;
        // }
        const slicedPages = _.slice(activePages, offset, offset + limit);
        // console.log(
        //     "activePages, slicedPages",
        //     activePages.length,
        //     slicedPages.length,
        //     offset,
        //     limit
        // );
        let hasMore = true;
        if (limit + offset >= activePages.length) {
            hasMore = false;
        }
        return res.status(200).json({
            message: "data portion loaded",
            payload: {
                landing: landing,
                slicedPages: slicedPages,
                hasMore: hasMore,
                lastLoaded: offset + limit,
                total: activePages.length,
            },
        });
    } catch (error) {
        console.log(error);
        return res
            .status(500)
            .json({ error: "internal error loading data", payload: error });
    }
});

module.exports = router;
