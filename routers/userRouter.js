// const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
const { OAuth2Client } = require("google-auth-library");
const jwt = require("jsonwebtoken");
// const mailgun = require("mailgun-js");
const _ = require("lodash");
const shortId = require("shortid");
const verifyJWT = require("./verifyJwt.js");

const nodemailer = require("nodemailer");

let transporter = nodemailer.createTransport({
    host: "smtp.mail.ru",
    port: 465,
    secure: true, // true for 465, false for other ports
    // rejectUnauthorized: false,
    auth: {
        user: "candylanding@mail.ru", // generated ethereal user
        // pass: "20302030", // generated ethereal password
        pass: "Zinka2030#", // generated ethereal password
    },
});

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const User = require("../models/user.js");
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// const DOMAIN = "sandboxe0d3faf11d3b47eeaa9126e51fed8f6e.mailgun.org";
// const mg = mailgun({ apiKey: process.env.MAILGUN_APIKEY, domain: DOMAIN });
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// **********************************************************************************************
// 196001869120-r8at099bqvb727793pe27erplth0as6o.apps.googleusercontent.com
// const clientId =
//     "196001869120-r8at099bqvb727793pe27erplth0as6o.apps.googleusercontent.com";
const clientId = process.env.GOOGLE_LOGIN_CLIENT_ID;
const gClient = new OAuth2Client(clientId);
// **********************************************************************************************

router.post("/signup", async (req, res) => {
    const { name, email, password } = req.body;
    console.log("signup");
    console.log("signup2");

    User.findOne({ email }).exec((err, user) => {
        if (err) {
            return res.status(400).json({
                error:
                    "Searching your email in database is failed. Try one more time.",
            });
        } else {
            if (user) {
                return res.status(400).json({
                    error: "User with this email is already exist!",
                });
            }
            const token = jwt.sign(
                { name, email, password },
                process.env.JWT_ACC_ACTIVATE,
                { expiresIn: "30m" }
            );

            console.log("ready to send");

            const options_ = {
                from: "Candy Landing<candylanding@mail.ru>",
                to: email,
                subject: "Account activation link",
                text:
                    "If link is not clickable, please, copy url and paste it into a browser address field to activate your account: " +
                    `${process.env.CLIENT_URL}/api/activate/${token}`,
                html: `
                    <h2>Please, click to activate your account:</h2>
                    <a href="${process.env.CLIENT_URL}/api/activate/${token}">${process.env.CLIENT_URL}/api/activate/${token}</a>
                `,
            };

            transporter.sendMail(options_, (error, info) => {
                if (error) {
                    console.log("signup3");
                    console.log(error);
                    return res
                        .status(400)
                        .json({ error: JSON.stringify(error) });
                } else {
                    return res.status(200).json({
                        message:
                            "I have sent an email to you. Please, kindly activate your account from email. (Check spam folder if not get the email)",
                    });
                }
            });
        }
    });
});

// **********************************************************************************************
router.post("/email-activate", async (req, res) => {
    const { token } = req.body;
    console.log(token);
    if (token) {
        jwt.verify(
            token,
            process.env.JWT_ACC_ACTIVATE,
            function (error, decodedToken) {
                if (error) {
                    return res.status(400).json({
                        error: "Token (link) is wrong or has already expired",
                    });
                }
                const { name, email, password } = decodedToken;
                User.findOne({ email }).exec((err, user) => {
                    if (err) {
                        return res.status(400).json({
                            error: "Activation is faild. Try one more time.",
                        });
                    } else {
                        if (user) {
                            return res.status(400).json({
                                error: "User with this email is already exist!",
                            });
                        } else {
                            let newUser = new User({ name, email, password });
                            newUser.save((err, success) => {
                                if (err) {
                                    console.log("error in sign up", err);
                                    return res.status(400).json({
                                        error:
                                            "Signing up is failed during activation. Try one more time.",
                                    });
                                } else {
                                    return res.status(200).json({
                                        message:
                                            "Sign up success. Now you can login",
                                    });
                                }
                            });
                        }
                    }
                });
            }
        );
    } else {
        return res.status(400).json({ error: "I did not get token" });
    }
});
// **********************************************************************************************
router.post("/forgot", async (req, res) => {
    const { email } = req.body;

    User.findOne({ email }).exec((err, user) => {
        if (err) {
            return res.status(400).json({
                error:
                    "Searching your email in database is failed. Try one more time.",
            });
        } else {
            if (!user) {
                return res.status(400).json({
                    error: "User with this email does not  exist!",
                });
            }
            const token = jwt.sign(
                { _id: user._id },
                process.env.JWT_PWD_RESET,
                { expiresIn: "30m" }
            );

            const link = `${process.env.CLIENT_URL}/api/reset/${token}`;
            console.log(token);
            return user.updateOne({ resetLink: token }, (err, success) => {
                if (err) {
                    return res.status(400).json({
                        error:
                            "Problem creating link to reset password. Try one more time.",
                    });
                }
                const options_ = {
                    from: "Candy Landing<candylanding@mail.ru>",
                    to: email,
                    subject: "Password reset link",
                    text:
                        "If link is not clickable, please, copy url and paste it into a browser address field to activate your password recovery: " +
                        link,
                    html: `
                        <h2>Please, click to reset you password:</h2>
                        <a href="${link}" >${link}</a>
                    `,
                };

                transporter.sendMail(options_, (error, info) => {
                    if (error) {
                        console.log(error);
                        return res
                            .status(400)
                            .json({ error: JSON.stringify(error) });
                    } else {
                        return res.status(200).json({
                            message:
                                "I have sent an email to you. Please, kindly visit link from email. (Check spam folder if not get the email)",
                        });
                    }
                });
            });
        }
    });
});
// **********************************************************************************************
// **********************************************************************************************
router.post("/reset", async (req, res) => {
    //
    const { resetLink, newPassword } = req.body;
    if (resetLink) {
        jwt.verify(resetLink, process.env.JWT_PWD_RESET, (err, decodedId) => {
            if (err) {
                return res.status(400).json({
                    error: "ResetLink is wrong or has already expired",
                });
            }
            User.findOne({ resetLink: resetLink }, (err, user) => {
                if (err || !user) {
                    return res
                        .status(400)
                        .json({ error: "User with this token does not exist" });
                }
                user = _.extend(user, { password: newPassword, resetLink: "" });
                user.save((err, success) => {
                    if (err) {
                        // console.log("error in sign up", err);
                        return res.status(400).json({
                            error:
                                "Error changing password. Try one more time.",
                        });
                    } else {
                        return res
                            .status(200)
                            .json({ message: "Password has been changed!!" });
                    }
                });
            });
        });
    } else {
        return res.status(401).json({
            error: "There is no reset link received. Try reset one more time.",
        });
    }
});
// **********************************************************************************************
router.post("/signin", async (req, res) => {
    //
    const { email, password } = req.body;
    User.findOne({ email }).exec((err, user) => {
        if (!user || err) {
            return res.status(400).json({ error: "Wrong credentials." });
        }
        if (user.password !== password) {
            return res.status(400).json({ error: "Wrong credentials" });
        }
        const token = jwt.sign(
            { _id: user._id },
            process.env.JWT_SIGNIN_TOKEN,
            { expiresIn: "7d" }
        );
        const { _id, name, email, accountSum } = user;
        return res.status(200).json({
            auth: true,
            token,
            user: { _id, name, email, accountSum },
            message: "SignIn is successfull",
        });
    });
});
// **********************************************************************************************

router.post("/googlelogin", async (req, res) => {
    const { tokenId } = req.body;
    console.log("googlelogin", "req.body", req.body);
    console.log("googlelogin", "clientId", clientId);

    if (!tokenId)
        return res.status(400).json({ error: "Token ID does not provided" });
    gClient
        .verifyIdToken({ idToken: tokenId, audience: clientId })
        .then((response) => {
            const { email_verified, name, email } = response.payload;
            // console.log(response.payload);
            if (email_verified) {
                User.findOne({ email }).exec((err, user) => {
                    if (err) {
                        return res.status(400).json({
                            error:
                                "Searching your email in database is failed. Try one more time.",
                        });
                    } else {
                        if (user) {
                            const token = jwt.sign(
                                { _id: user._id },
                                process.env.JWT_SIGNIN_TOKEN,
                                { expiresIn: "7d" }
                            );
                            const { _id, name, email, accountSum } = user;
                            return res.status(200).json({
                                auth: true,
                                token,
                                user: { _id, name, email, accountSum },
                                message: "SignIn is successfull",
                            });
                        } else {
                            const password = shortId.generate();
                            const newUser = new User({ name, email, password });
                            newUser.save((err, data) => {
                                if (err) {
                                    return res.status(400).json({
                                        error:
                                            "Problem to save user to database. Google credentials",
                                    });
                                }
                                const token = jwt.sign(
                                    { _id: data._id },
                                    process.env.JWT_SIGNIN_TOKEN,
                                    { expiresIn: "7d" }
                                );
                                const { _id, name, email } = data;
                                return res.status(200).json({
                                    auth: true,
                                    token,
                                    user: { _id, name, email, accountSum: 0 },
                                    message: "SignUp is successfull",
                                });
                            });
                        }
                    }
                });
            }
        })
        .catch((err) => {
            console.log(err);
        });
});

router.post("/updateUserInfo", verifyJWT, async (req, res) => {
    try {
        const { userId } = req.body;
        const user = await User.findOne({ _id: userId });
        const { _id, name, email, accountSum } = user;
        return res.status(200).json({
            user: { _id, name, email, accountSum },
            message: "User updated successfully",
        });
    } catch (error) {
        return res.status(400).json({
            error: "Problem upateUserInfo",
        });
    }
});

module.exports = router;
