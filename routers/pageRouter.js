// const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const shortId = require("shortid");
const verifyJWT = require("./verifyJwt.js");

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const Page = require("../models/page.js");
const Landing = require("../models/landing.js");

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// **********************************************************************************************
// 196001869120-r8at099bqvb727793pe27erplth0as6o.apps.googleusercontent.com

// **********************************************************************************************

router.post("/createPage", verifyJWT, async (req, res) => {
    const { landingId, userId } = req.body;

    try {
        const landing = await Landing.findOne({ _id: landingId });

        if (!landing) {
            console.log(
                "some stupid error. check 3487234987",
                landingId,
                userId
            );
            return res.status(400).json({
                error: "some stupid error. check 3487234987",
                landingId,
                userId,
            });
        }

        const newPage = new Page({
            user: userId,
            landing: landingId,
        });
        const result = await newPage.save();

        landing.pages.push({ _id: result._id, order: landing.pagesCount++ });
        if (landing.pages.length > 3) {
            landing.totalCost = landing.pages.length * 0.05;
        }
        landing.save();
        console.log(result);
        return res.status(200).json({ message: "Page  was created" });
    } catch (error) {
        console.log(error);
        return res
            .status(400)
            .json({ error: "Problem creating page", landingId, userId });
    }
});

router.post("/getListOfPages", verifyJWT, async (req, res) => {
    const { landingId, userId } = req.body;

    try {
        const pages = await Page.find({ user: userId, landing: landingId });

        if (!pages) {
            console.log(
                "some stupid error. check 1231231242345fdg",
                landingId,
                userId
            );
            return res.status(400).json({
                error: "some stupid error. check 1231231242345fdg",
                landingId,
                userId,
            });
        }

        console.log("pages.count", pages.count);
        return res
            .status(200)
            .json({ message: "Pages are loaded", payload: pages });
    } catch (error) {
        console.log(error);
        return res
            .status(400)
            .json({ error: "Problem loading page", landingId, userId });
    }
});

router.post("/saveParticularPage", verifyJWT, async (req, res) => {
    console.log("saveParticularPage");
    const { landingId, userId, pageId, updatedPage } = req.body;

    try {
        let page = await Page.findOne({
            user: userId,
            landing: landingId,
            _id: pageId,
        });

        if (!page) {
            console.log(
                "some stupid error. check 9438573495 asfdaf",
                landingId,
                userId,
                pageId
            );
            return res.status(400).json({
                error: "some stupid error. check 9438573495 asfdaf",
                landingId,
                userId,
                pageId,
            });
        }
        page = _.extend(page, updatedPage);
        await page.save();

        const pages = await Page.find({ user: userId, landing: landingId });

        if (!pages) {
            console.log(
                "some stupid error. check 1231231242345fdg",
                landingId,
                userId
            );
            return res.status(400).json({
                error: "some stupid error. check 1231231242345fdg",
                landingId,
                userId,
            });
        }

        console.log("pages.count", pages.count);
        return res
            .status(200)
            .json({ message: "Pages are loaded", payload: pages });
    } catch (error) {
        console.log(error);
        return res
            .status(400)
            .json({ error: "Problem savintg page", landingId, userId });
    }
});

router.post("/incrementLikes", async (req, res) => {
    console.log("\n\n\n\n\nincrementLikes post");
    const { landingId } = req.body;
    console.log("incrementLikes post", landingId);
    try {
        const pages = await Page.updateMany(
            { landing: landingId },
            { $inc: { likes: 1 } }
        );

        return res.status(200).json({ message: "likes has been updated!" });
    } catch (error) {
        console.log(error);
        return res
            .status(500)
            .json({ error: "internal error likes updating", payload: error });
    }
});

router.post("/incrementShares", async (req, res) => {
    console.log("\n\n\n\n\nincrementShares post");
    const { landingId } = req.body;
    console.log("incrementShares post", landingId);
    try {
        const pages = await Page.updateMany(
            { landing: landingId },
            { $inc: { shares: 1 } }
        );

        return res.status(200).json({ message: "shares has been updated!" });
    } catch (error) {
        console.log(error);
        return res
            .status(500)
            .json({ error: "internal error shares updating", payload: error });
    }
});

module.exports = router;
