// const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const shortId = require("shortid");

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const Payment = require("../models/payment.js");
const user = require("../models/user.js");
const User = require("../models/user.js");
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// **********************************************************************************************

// **********************************************************************************************

router.post("/writeCheckout", async (req, res) => {
    const { userId, amount, status, currency, payload } = req.body;
    console.log("writeCheckout");
    try {
        const payment = new Payment({
            userId: userId,
            amount: amount,
            status: status,
            payload: payload,
            currency: currency,
        });
        await payment.save();
        let user = await User.findOne({ _id: userId });
        const newAmount = Number(user.accountSum) + Number(amount);
        user = _.extend(user, { accountSum: newAmount });
        await user.save();
        return res.status(200).json({ message: "Payment completed! Congrads" });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            error:
                "Payment problem! Please photo information shown below and contact to me via Telegram @tverdn",
            payload: payload,
        });
    }
});

module.exports = router;
