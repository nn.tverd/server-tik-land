const mongoose = require("mongoose");

// mongo has been create on ains.comp@gmail.com
mongoose
    .connect(process.env.DATABASE, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: true,
        useCreateIndex: true,
    })
    .then(() => {
        console.log("DB connection is istablished");
    })
    .catch((err) => {
        console.log("DB connection error", err);
    });
